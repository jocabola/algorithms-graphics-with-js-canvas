// 1: Fetch the DIV from the DOM
var container = document.querySelector(".container");
        
// 2: Create Canvas
var canvas = document.createElement("canvas");
canvas.width = canvas.height = 512;
container.appendChild(canvas);

// 3: Get Canvas 2D Context
var ctx = canvas.getContext('2d');

// 4: Our algorithm starts here
console.log("^^__^^ Hi!");

var mouseCoords = {
    x: -1000,
    y: -1000
}

function Circle(x, y, radius) {
    this.x = x;
    this.y = y;
    this.radius = radius;

    this.setPosition = function(x,y) {
        this.x = x;
        this.y = y;
    }
}

function onMouseMove( event ) {
    var rect = canvas.getBoundingClientRect();
    mouseCoords.x = event.x - rect.x;
    mouseCoords.y = event.y - rect.y;
}

window.addEventListener("mousemove", onMouseMove);

ctx.fillStyle = "#000";

function animate () {
    requestAnimationFrame(animate);

    ctx.clearRect(0,0,512,512);
    ctx.beginPath();
    ctx.arc(mouseCoords.x, mouseCoords.y, 30, 0, 2*Math.PI);
    ctx.fill();
}

// 5: launch animation
animate();