// 1: Fetch the DIV from the DOM
var container = document.querySelector(".container");
        
// 2: Create Canvas
var canvas = document.createElement("canvas");
canvas.width = canvas.height = 512;
container.appendChild(canvas);

// 3: Get Canvas 2D Context
var ctx = canvas.getContext('2d');

// 4: Our algorithm starts here
console.log("^^__^^ Hi!");

var img = new Image();
img.onload = function () {
    console.log("Image loaded");

    drawImage();
}
img.src = "img/landscape.jpg";

var pixelData;

function drawImage() {
    ctx.drawImage(img, 0, 0, 512, 512);
    pixelData = ctx.getImageData(0,0,512,512);
    //swapRB();
    //grayscale();
}

function swapRB() {
    var nPixels = pixelData.data.length/4;
    for ( var i=0; i<nPixels; i++ ) {
        var r = pixelData.data[i*4];
        var g = pixelData.data[i*4+1];
        var b = pixelData.data[i*4+2];
        var a = pixelData.data[i*4+3];

        pixelData.data[i*4] = b;
        pixelData.data[i*4+2] = r;
    }

    ctx.putImageData(pixelData, 0, 0);
}

function grayscale() {
    var nPixels = pixelData.data.length/4;
    for ( var i=0; i<nPixels; i++ ) {
        var r = pixelData.data[i*4];
        var g = pixelData.data[i*4+1];
        var b = pixelData.data[i*4+2];
        var a = pixelData.data[i*4+3];

        var gray = (r+b+g)/3;

        pixelData.data[i*4] = gray;
        pixelData.data[i*4+1] = gray;
        pixelData.data[i*4+2] = gray;
    }

    ctx.putImageData(pixelData, 0, 0);
}