// 1: Fetch the DIV from the DOM
var container = document.querySelector(".container");
        
// 2: Create Canvas
var canvas = document.createElement("canvas");
canvas.width = canvas.height = 512;
container.appendChild(canvas);

// 3: Get Canvas 2D Context
var ctx = canvas.getContext('2d');

// 4: Our algorithm starts here
console.log("^^__^^ Hi!");

function drawSquareFromCenter( x, y, siz ) {
    ctx.save();
    ctx.translate(x,y);
    ctx.rect(-siz/2,-siz/2,siz,siz);
    ctx.restore();
}

function drawTransformedSquare( x, y, siz, rot, scale ) {
    ctx.save();
    ctx.translate(x,y);
    ctx.rotate(rot);
    ctx.scale(scale,scale);
    ctx.rect(-siz/2,-siz/2,siz,siz);
    ctx.restore();
}

ctx.strokeStyle = "#000";
ctx.lineWidth = 2;

ctx.beginPath();

drawSquareFromCenter(256,256,48);

drawTransformedSquare(256,256,48, Math.PI/4, 5);

ctx.stroke();